var database = require('../config/database.config');
var tarea = {}

tarea.selectAll = function(callback) {
  if(database) {
    database.query('call sp_selectTarea()',
    function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados[0]);
      } else {
        callback(0);
      }
    })
  }
}

tarea.find = function(idTarea, callback) {
  if(database) {
    database.query('select * from tarea where idTarea = ?', [idTarea], function(error, resultados) {
      if(error) throw error;
      if(resultados.length > 0) {
        callback(resultados);
      } else {
        callback(0);
      }
    })
  }
}

tarea.insert = function(data, callback) {
  if(database) {
    var values = [data.idTarea, data.titulo, data.descripcion, data.fechaFinal]
    database.query("call sp_insertTarea(?,?,?,?)", values,
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

tarea.update = function(data, callback) {
  if(database) {
    var values = [data.titulo, data.descripcion, data.fechaFinal, data.idTarea]
    var sql = "UPDATE Tarea set titulo = ?, descripcion = ?, fecha_final = ? where idTarea = ?";
    database.query(sql, values,
    function(error, resultado) {
      if(error) throw error;
      callback(resultado);
    });
  }
}

tarea.delete = function(idTarea, callback) {
  if(database) {
    var sql = "DELETE Tarea from Tarea WHERE idTarea = ? ";
    database.query(sql, idTarea, function(error, resultado) {
      if(error) throw error;
      callback({"Mensaje": "Eliminado"});
    });
  }
}



module.exports = tarea;
