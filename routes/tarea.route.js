var express = require('express');
var TareaModel = require('../model/tarea.model');
var TareaRoute = express.Router();

TareaRoute.route('/api/v1/tarea/')
  .get(function(req, res) {
    TareaModel.selectAll(function(resultados) {
      if(resultados !== 0) {
        res.json(resultados);
      }
    });
  })
  .post(function(req, res) {
    var data = {
      idUsuario: req.body.idUsuario,
      titulo: req.body.titulo,
      descripcion: req.body.descripcion,
      fechaFinal: req.body.fechaFinal
    }

    TareaModel.insert(data, function(resultado){
        res.json(data);
    });
  });

TareaRoute.route('/api/v1/Tarea/:idTarea')
  .get(function(req, res) {
    var idTarea = req.params.idTarea;
    TareaModel.find(idTarea, function(resultados){
      if(typeof resultados !== undefined) {
        res.json(resultados);
      } else {
        res.json({"Mensaje": "No se encontro el Tarea"})
      }
    });
  })

  .put(function(req, res) {

    var data = {
      idTarea : req.params.idTarea,
      titulo: req.body.titulo,
      descripcion: req.body.descripcion,
      fechaFinal: req.body.fechaFinal
    }
      TareaModel.update(data, function(resultado){
        res.json(resultado);
      })
    
  })

  .delete(function(req, res) {
    var idTarea = req.params.idTarea;
    TareaModel.delete(idTarea, function(resultados){
        res.json(resultados);
    });
  });


module.exports = TareaRoute;
